FROM node:18-alpine AS build

WORKDIR /app

COPY package*.json ./
RUN npm install

COPY . .
RUN npm run build

FROM nginx:stable-alpine

COPY --from=build /app/dist/eq-presentation /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 82

CMD ["nginx", "-g", "daemon off;"]
